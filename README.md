# Node Express Learn Config Dev

## GitOps companion repository
For the `dev` deployment environment of the app: [Node-Express-Learn](https://gitlab.com/miltozz/node-express-learn/-/tree/main) 

## K8s manifests for ne-mvp app
- Some sample k8s manifests to create resources in killercoda k8s cluster for the ne-mvp app.
- Also used with ArgoCD GitOps in [this](https://killercoda.com/mabusaa/course/argocd-endusers-scenarios/00-argocd-playground) environment.

## K8s resources to create

Resources:
- app deploy and svc
- db deploy, svc and configMap for init db password
--- 

